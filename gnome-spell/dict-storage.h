#ifndef _DICT_STORAGE_H_
#define _DICT_STORAGE_H_

#include <bonobo/bonobo-storage.h>

BEGIN_GNOME_DECLS

#define BONOBO_STORAGE_DICT_TYPE        (bonobo_storage_dict_get_type ())
#define BONOBO_STORAGE_DICT(o)          (GTK_CHECK_CAST ((o), BONOBO_STORAGE_DICT_TYPE, BonoboStorageDict))
#define BONOBO_STORAGE_DICT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_STORAGE_DICT_TYPE, BonoboStorageDictClass))
#define BONOBO_IS_STORAGE_DICT(o)       (GTK_CHECK_TYPE ((o), BONOBO_STORAGE_DICT_TYPE))
#define BONOBO_IS_STORAGE_DICT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_STORAGE_DICT_TYPE))

typedef struct {
	BonoboStorage storage;
} BonoboStorageDict;

typedef struct {
	BonoboStorageClass parent_class;
} BonoboStorageDictClass;

GtkType        bonobo_storage_dict_get_type  (void);
BonoboStorage *bonobo_storage_dict_construct (BonoboStorageDict *storage,
					      Bonobo_Storage corba_storage,
					      const char *path, const char *open_mode);
BonoboStorage *bonobo_storage_dict_open      (const char *path,
					      gint flags, gint mode,
					      CORBA_Environment *ev);
BonoboStorage *bonobo_storage_dict_create    (BonoboStorageDict *storage,
					      const CORBA_char *path);

END_GNOME_DECLS

#endif /* _BONOBO_STORAGE_DICT_H_ */
