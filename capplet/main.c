/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* This file is part of the GNOME Spell

   Copyright (C) 2002 Ximian, Inc.
   Authors:           Radek Doulik (rodo@ximian.com)

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHcANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <config.h>
#include <string.h>
#include <gnome.h>
#include <gconf/gconf-client.h>
#include <capplet-widget.h>
#include <bonobo.h>
#include <glade/glade.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkcellrenderer.h>

#include "../gnome-spell/Spell.h"

#define GNOME_SPELL_GCONF_DIR "/GNOME/Spell"

static CORBA_sequence_GNOME_Spell_Language *seq;

static GtkWidget *capplet;
static GtkWidget *language_list;
static GtkWidget *color_picker;
static GtkListStore *store;

static gboolean active = FALSE;

static GError      *error  = NULL;
static GConfClient *client = NULL;

static gchar *language_str = NULL, *language_str_orig = NULL;
static GdkColor spell_error_color, spell_error_color_orig;

static void
select_lang (GtkTreeSelection *selection, const gchar *abrev)
{
	GtkTreeIter iter;
	gint i;
	gchar *row;

	for (i = 0; i < seq->_length; i ++) {
		if (!strcasecmp (abrev, seq->_buffer [i].abrev)) {
			/* printf ("select %s\n", abrev); */
			row = g_strdup_printf ("%d", i);

			if (gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL (store), &iter, row))
				gtk_tree_selection_select_iter (selection, &iter);
			g_free (row);
		}
	}
}

static void
set_ui_language ()
{
	GtkTreeSelection *selection;
	gchar *l, *last, *lang;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (language_list));
	gtk_tree_selection_unselect_all (selection);

	/* printf ("language %s\n", language_str); */

	last = language_str;
	while ((l = strchr (last, ' '))) {
		if (l != last) {
			lang = g_strndup (last, l - last);
			select_lang (selection, lang);
			g_free (lang);
		}

		last = l + 1;
	}
	if (last)
		select_lang (selection, last);
}

static void
set_ui ()
{
	active = FALSE;

	set_ui_language ();
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (color_picker),
				    spell_error_color.red, spell_error_color.green, spell_error_color.blue, 0xffff);

	active = TRUE;
}

void
add_selected (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	GString *str = data;
	gchar *abrev;

	gtk_tree_model_get (model, iter, 1, &abrev, -1);
	if (str->str)
		g_string_append_c (str, ' ');
	g_string_append (str, abrev);
}

static gchar *
get_language_str ()
{
	GString *str = g_string_new (NULL);
	gchar *rv;

	gtk_tree_selection_selected_foreach (gtk_tree_view_get_selection (GTK_TREE_VIEW (language_list)), add_selected, str);
					     
	rv = str->str;
	g_string_free (str, FALSE);

	return rv;
}

static void
get_ui ()
{
	gnome_color_picker_get_i16 (GNOME_COLOR_PICKER (color_picker),
				    &spell_error_color.red, &spell_error_color.green, &spell_error_color.blue, NULL);
	g_free (language_str);
	language_str = get_language_str ();
}

#define GET(t,x,prop,f,c) \
        val = gconf_client_get_without_default (client, GNOME_SPELL_GCONF_DIR x, NULL); \
        if (val) { f; prop = c (gconf_value_get_ ## t (val)); \
        gconf_value_free (val); }

static void
save_orig ()
{
	g_free (language_str_orig);
	language_str_orig = g_strdup (language_str);
	spell_error_color_orig = spell_error_color;
}

static void
load_orig ()
{
	g_free (language_str);
	language_str = g_strdup (language_str_orig);
	spell_error_color = spell_error_color_orig;
}

static void
load_values ()
{
	GConfValue *val;

	g_free (language_str);
	language_str = g_strdup (_("en"));
	spell_error_color.red   = 0xffff;
	spell_error_color.green = 0;
	spell_error_color.blue  = 0;

	GET (int, "/spell_error_color_red",   spell_error_color.red,,);
	GET (int, "/spell_error_color_green", spell_error_color.green,,);
	GET (int, "/spell_error_color_blue",  spell_error_color.blue,,);
	GET (string, "/language", language_str, g_free (language_str), g_strdup);

	save_orig ();
}

#define SET(t,x,prop) \
        gconf_client_set_ ## t (client, GNOME_SPELL_GCONF_DIR x, prop, NULL);

static void
save_values (gboolean force)
{
	GConfValue *val;

	if (force || !gdk_color_equal (&spell_error_color, &spell_error_color_orig)) {
		SET (int, "/spell_error_color_red",   spell_error_color.red);
		SET (int, "/spell_error_color_green", spell_error_color.green);
		SET (int, "/spell_error_color_blue",  spell_error_color.blue);
	}
	if (force || strcmp (language_str, language_str_orig)) {
		SET (string, "/language", language_str);
	}

	gconf_client_suggest_sync (client, NULL);
}

static void
apply (void)
{
	get_ui ();
	save_values (FALSE);
}

static void
revert (void)
{
	load_orig ();
	set_ui ();
	save_values (TRUE);
}

static void
changed (GtkWidget *widget, gpointer null)
{
	if (active)
		capplet_widget_state_changed (CAPPLET_WIDGET (capplet), TRUE);
}

static void
setup ()
{
	GtkWidget *hbox, *ebox;
	GladeXML *xml;
	GtkTreeIter iter;
	gint i;

	glade_gnome_init ();
	xml = glade_xml_new (GLADE_DATADIR "/gnome-spell-capplet.glade", "prefs_widget", NULL);

	if (!xml)
		g_error (_("Could not load glade file."));

        capplet = capplet_widget_new();
	hbox    = glade_xml_get_widget (xml, "prefs_widget");

	language_list = glade_xml_get_widget (xml, "clist_language");
	store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (language_list), GTK_TREE_MODEL (store));
	
	gtk_tree_view_append_column (GTK_TREE_VIEW (language_list),
				     gtk_tree_view_column_new_with_attributes (_("Languages"),
									       gtk_cell_renderer_text_new (),
									       "text", 0, NULL));
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (language_list)), GTK_SELECTION_MULTIPLE);
	g_signal_connect (gtk_tree_view_get_selection (GTK_TREE_VIEW (language_list)), "changed", G_CALLBACK (changed), NULL);

	for (i = 0; i < seq->_length; i ++) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 0, seq->_buffer [i].name, 1, seq->_buffer [i].abrev, -1);
	}

	color_picker = glade_xml_get_widget (xml, "color_picker");

        gtk_container_add (GTK_CONTAINER (capplet), hbox);
        gtk_widget_show_all (capplet);

	load_values ();
	set_ui ();

	glade_xml_signal_connect (xml, "changed", G_CALLBACK (changed));
}

int
main (int argc, char **argv)
{
	GNOME_Spell_Dictionary dict;
	CORBA_Environment ev;
	gchar *dictionary_id;

        bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
        textdomain (GETTEXT_PACKAGE);

        if (gnome_capplet_init ("gnome-spell-properties", VERSION, argc, argv, NULL, 0, NULL) < 0)
		return 1;

	bonobo_init (&argc, argv);
	bonobo_activate ();
	dict = bonobo_get_object ("OAFIID:GNOME_Spell_Dictionary:" API_VERSION, "GNOME/Spell/Dictionary", NULL);

	if (dict == CORBA_OBJECT_NIL)
		g_error ("Cannot activate dictionary");

	CORBA_exception_init (&ev);
	seq = GNOME_Spell_Dictionary_getLanguages (dict, &ev);
	CORBA_exception_free (&ev);

	client = gconf_client_get_default ();
	gconf_client_add_dir (client, GNOME_SPELL_GCONF_DIR, GCONF_CLIENT_PRELOAD_NONE, NULL);

        setup ();

	/* connect signals */
        g_signal_connect (capplet, "try", G_CALLBACK (apply), NULL);
        g_signal_connect (capplet, "revert", G_CALLBACK (revert), NULL);
        g_signal_connect (capplet, "ok", G_CALLBACK (apply), NULL);
        g_signal_connect (capplet, "cancel", G_CALLBACK (revert), NULL);

        capplet_gtk_main ();

	bonobo_object_release_unref (dict, NULL);

        return 0;
}
