/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  This file is part of gnome-spell bonobo component
    copied from echo.c written by Miguel de Icaza and updated for Spell.idl needs

    Copyright (C) 1999, 2000 Helix Code, Inc.
    Authors:                 Radek Doulik <rodo@helixcode.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include <config.h>
#include <bonobo.h>
#include <gtkhtml/gtkhtml.h>
#include <glade/glade.h>

#include "control.h"
#include "engine.h"

static BonoboObjectClass                  *engine_parent_class;
static POA_GNOME_Spell_Engine__vepv    engine_vepv;

static void next_word (GNOMESpellEngine *e, CORBA_Environment * ev);

static void
unref_dictionary (GNOMESpellEngine *e)
{
	if (e->dictionary_client)
		bonobo_object_unref (BONOBO_OBJECT (e->dictionary_client));
}

static void
unref_client (GNOMESpellEngine *e)
{
	if (e->client_client)
		bonobo_object_unref (BONOBO_OBJECT (e->client_client));
}

static void
engine_object_init (GtkObject *object)
{
	GNOMESpellEngine *dict = GNOME_SPELL_ENGINE (object);

	dict->dialog            = NULL;
	dict->dictionary_client = NULL;
	dict->client_client     = NULL;
	dict->dictionary        = CORBA_OBJECT_NIL;
	dict->client            = CORBA_OBJECT_NIL;
}

static void
engine_object_destroy (GtkObject *object)
{
	GNOMESpellEngine *engine = GNOME_SPELL_ENGINE (object);

	unref_dictionary (engine);
	unref_client (engine);

	if (engine->words_db_client)
		bonobo_object_unref (BONOBO_OBJECT (engine->words_db_client));

	GTK_OBJECT_CLASS (engine_parent_class)->destroy (object);
}

/**
 * methods implementation
 */

inline static GNOMESpellEngine *
engine_from_servant (PortableServer_Servant _servant)
{
	return GNOME_SPELL_ENGINE (bonobo_object_from_servant (_servant));
}

static GNOME_Spell_Engine
impl_Engine (PortableServer_Servant _servant, const GNOME_Spell_Client client,
	     const GNOME_Spell_Dictionary dictionary, CORBA_Environment * ev)
{
}

static void
impl_set_dictionary (PortableServer_Servant _servant, const GNOME_Spell_Dictionary dictionary, CORBA_Environment * ev)
{
	GNOMESpellEngine *e = engine_from_servant (_servant);

	unref_dictionary (e);

	e->dictionary_client = bonobo_object_client_from_corba (dictionary);
	e->dictionary        = bonobo_object_client_query_interface (e->dictionary_client,
								     "IDL:GNOME/Spell/Dictionary:1.0", ev);
}

static void
impl_set_client (PortableServer_Servant _servant, const GNOME_Spell_Client client, CORBA_Environment * ev)
{
	GNOMESpellEngine *e = engine_from_servant (_servant);

	unref_client (e);

	e->client_client = bonobo_object_client_from_corba (client);
	e->client        = bonobo_object_client_query_interface (e->client_client,
								 "IDL:GNOME/Spell/Client:1.0", ev);
}

static void
replace (GtkWidget *w, GNOMESpellEngine *e)
{
	CORBA_Environment ev;
	gchar *replacement;

	CORBA_exception_init (&ev);
	g_assert (gtk_clist_get_text (GTK_CLIST (e->suggestions_clist), e->selected_row, 0, &replacement));
	GNOME_Spell_Dictionary_setCorrection (e->dictionary, gtk_entry_get_text (GTK_ENTRY (e->word_entry)), replacement, &ev);
	GNOME_Spell_Client_replace (e->client, replacement, &ev);
	
	next_word (e, &ev);
}

static void
remember (GtkWidget *w, GNOMESpellEngine *e)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	GNOME_Spell_Dictionary_addWordToPersonal (e->dictionary, gtk_entry_get_text (GTK_ENTRY (e->word_entry)), &ev);
	GNOME_Spell_Client_remembered (e->client, &ev);
	next_word (e, &ev);
	CORBA_exception_free (&ev);
}

static void
ignore (GtkWidget *w, GNOMESpellEngine *e)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	GNOME_Spell_Dictionary_addWordToSession (e->dictionary, gtk_entry_get_text (GTK_ENTRY (e->word_entry)), &ev);
	GNOME_Spell_Client_ignored (e->client, &ev);
	next_word (e, &ev);
	CORBA_exception_free (&ev);
}

static void
next (GtkWidget *w, GNOMESpellEngine *e)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	next_word (e, &ev);
	CORBA_exception_free (&ev);
}

static void
cancel (GtkWidget *w, GNOMESpellEngine *e)
{
	gnome_dialog_close (GNOME_DIALOG (e->dialog));
	gtk_main_quit ();
}

static void
clear_words (GNOMESpellEngine *e)
{
	gtk_clist_clear (GTK_CLIST (e->suggestions_clist));
	if (e->word_description) {
		for (; e->words > 0; e->words --)
			g_free (e->word_description [e->words - 1]);
		g_free (e->word_description);
		e->word_description = NULL;
	}
}

static void
fill_suggestions (GNOMESpellEngine *e, const gchar *word, CORBA_Environment * ev)
{
	GNOME_Spell_StringSeq *seq;
	const char * suggested_word [1];
	gchar *status;
	gint i;

	gtk_entry_set_text (GTK_ENTRY (e->word_entry), word);
	seq = GNOME_Spell_Dictionary_getSuggestions (e->dictionary, word, ev);

	status = g_strdup_printf ("Found %d possible corrections", seq->_length);
	gtk_label_set_text (GTK_LABEL (e->status_label), status);
	g_free (status);

	gtk_clist_freeze (GTK_CLIST (e->suggestions_clist));
	clear_words (e);

	if (ev->_major == CORBA_NO_EXCEPTION) {
		e->words = seq->_length;
		for (i=0; i < e->words; i++) {
			suggested_word [0] = seq->_buffer [i];
			gtk_clist_append (GTK_CLIST (e->suggestions_clist), (gchar **) suggested_word);
		}
		e->word_description = g_new0 (gchar *, e->words);
		CORBA_free (seq);
	}
	gtk_clist_thaw (GTK_CLIST (e->suggestions_clist));
}

static void
next_word (GNOMESpellEngine *e, CORBA_Environment * ev)
{
	gboolean free_mev = FALSE;
	gchar *word;

	g_assert (IS_GNOME_SPELL_ENGINE (e));
	g_assert (e->client);
	g_assert (e->dictionary);

	do {
		word = GNOME_Spell_Client_nextWord (e->client, ev);
		if (!*word)
			break;
		/* printf ("received word: '%s'\n", word); */
		if (!GNOME_Spell_Dictionary_checkWord (e->dictionary, word, ev))
			break;
	} while (*word);

	if (*word) {
		gchar *title;

		title = g_strdup_printf ("Spell check: %s", word);
		gtk_window_set_title (GTK_WINDOW (e->dialog), title);
		g_free (title);
		fill_suggestions (e, word, ev);
		gtk_widget_grab_focus (GTK_WIDGET (e->suggestions_clist));
	} else
		cancel (NULL, e);
}

static void
select_row (GtkWidget *w, gint row, gint column, GdkEvent *event, GNOMESpellEngine *e)
{
	/* printf ("selected row %d\n", row); */
	e->selected_row = row;
	if (e->word_description && e->word_description [row])
		gtk_html_load_from_string (GTK_HTML (e->description_gtkhtml), e->word_description [row], -1);
	else
		gtk_html_load_from_string (GTK_HTML (e->description_gtkhtml),
					   "<CENTER><BR><BR>"
					   "<FONT COLOR=\"#E0E0E0\" SIZE=\"+3\">Not loaded.</FONT>", -1);
}

static void
ensure_words_db (GNOMESpellEngine *e)
{
	if (!e->words_db_client) {
		e->words_db_client = bonobo_object_activate ("OAFIID:GNOME_Dictionary:" API_VERSION, 0);
		e->words_db        = bonobo_object_corba_objref (BONOBO_OBJECT (e->words_db_client));
	}
}

static void
get_description (GNOMESpellEngine *e, gint i)
{
	if (!e->word_description [i] && e->words_db) {
		gchar *text;

		if (gtk_clist_get_text (GTK_CLIST (e->suggestions_clist), i, 0, &text)) {
			CORBA_Environment ev;

			CORBA_exception_init (&ev);
			text = GNOME_Dictionary_lookup_html (e->words_db, text, &ev);
			if (ev._major == CORBA_NO_EXCEPTION) {
				e->word_description [i] = g_strdup (text);
				/* printf ("description: %s\n", text); */
			}
			CORBA_exception_free (&ev);
		}
	}
}

static void
describe (GtkWidget *w, GNOMESpellEngine *e)
{
	ensure_words_db (e);

	get_description (e, e->selected_row);
	gtk_html_load_from_string (GTK_HTML (e->description_gtkhtml), e->word_description [e->selected_row], -1);
}

static void
describe_all (GtkWidget *w, GNOMESpellEngine *e)
{
	gint i;
	ensure_words_db (e);

	for (i = 0; i < e->words; i++)
		get_description (e, i);

	gtk_html_load_from_string (GTK_HTML (e->description_gtkhtml), e->word_description [e->selected_row], -1);
}

static void
construct_dialog (GNOMESpellEngine *e)
{
	GladeXML  *xml;

	xml = glade_xml_new (gnome_spell_control_get_glade_file (), NULL);
	glade_xml_signal_autoconnect(xml);

	e->dialog              = glade_xml_get_widget (xml, "Spell dialog");
	e->word_entry          = glade_xml_get_widget (xml, "word_entry");
	e->suggestions_clist   = glade_xml_get_widget (xml, "suggestions_clist");
	e->status_label        = glade_xml_get_widget (xml, "status_label");
	e->sw_html             = glade_xml_get_widget (xml, "sw_html");
	e->description_gtkhtml = gtk_html_new ();

	gtk_container_add (GTK_CONTAINER (e->sw_html), e->description_gtkhtml);
	gtk_widget_show (e->description_gtkhtml);

	gnome_dialog_button_connect (GNOME_DIALOG (e->dialog), 0, replace, e);
	gnome_dialog_button_connect (GNOME_DIALOG (e->dialog), 1, remember, e);
	gnome_dialog_button_connect (GNOME_DIALOG (e->dialog), 2, ignore, e);
	gnome_dialog_button_connect (GNOME_DIALOG (e->dialog), 3, next, e);
	gnome_dialog_button_connect (GNOME_DIALOG (e->dialog), 4, cancel, e);

	gnome_dialog_set_default (GNOME_DIALOG (e->dialog), 0);

	gtk_signal_connect (GTK_OBJECT (e->suggestions_clist), "select_row", select_row, e);

	gtk_signal_connect (GTK_OBJECT (glade_xml_get_widget (xml, "describe_button")), "clicked", describe, e);
	gtk_signal_connect (GTK_OBJECT (glade_xml_get_widget (xml, "describe_all_button")), "clicked", describe_all, e);
}

static void
impl_run (PortableServer_Servant _servant, CORBA_Environment * ev)
{
	GNOMESpellEngine *e = engine_from_servant (_servant);

	if (!e->dialog)
		construct_dialog (e);
	next_word (e, ev);

	gtk_main ();
}

static void
impl_stop (PortableServer_Servant _servant, CORBA_Environment * ev)
{
}

/*
 * If you want users to derive classes from your implementation
 * you need to support this method.
 */
POA_GNOME_Spell_Engine__epv *
gnome_spell_engine_get_epv (void)
{
	POA_GNOME_Spell_Engine__epv *epv;

	epv = g_new0 (POA_GNOME_Spell_Engine__epv, 1);

	epv->Engine         = impl_Engine;
	epv->dictionarySet  = impl_set_dictionary;
	epv->clientSet      = impl_set_client;
	epv->run            = impl_run;
	epv->stop           = impl_stop;

	return epv;
}

static void
init_engine_corba_class (void)
{
	engine_vepv.Bonobo_Unknown_epv         = bonobo_object_get_epv ();
	engine_vepv.GNOME_Spell_Engine_epv = gnome_spell_engine_get_epv ();
}

static void
engine_class_init (GNOMESpellEngineClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	engine_parent_class  = gtk_type_class (bonobo_object_get_type ());
	object_class->destroy    = engine_object_destroy;

	init_engine_corba_class ();
}

GtkType
gnome_spell_engine_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"Engine",
			sizeof (GNOMESpellEngine),
			sizeof (GNOMESpellEngineClass),
			(GtkClassInitFunc) engine_class_init,
			(GtkObjectInitFunc) engine_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type (), &info);
	}

	return type;
}

GNOMESpellEngine *
engine_construct (GNOMESpellEngine *engine, GNOME_Spell_Engine corba_engine)
{
	CORBA_Environment ev;
	static int i;
	
	g_return_val_if_fail (engine != NULL, NULL);
	g_return_val_if_fail (IS_GNOME_SPELL_ENGINE (engine), NULL);
	g_return_val_if_fail (corba_engine != CORBA_OBJECT_NIL, NULL);

	if (!bonobo_object_construct (BONOBO_OBJECT (engine), (CORBA_Object) corba_engine))
		return NULL;

	CORBA_exception_init (&ev);
	CORBA_exception_free (&ev);

	return engine;
}

/*
 * This routine creates the ORBit CORBA server and initializes the
 * CORBA side of things
 */
static GNOME_Spell_Engine
create_engine (BonoboObject *engine)
{
	POA_GNOME_Spell_Engine *servant;
	CORBA_Environment ev;

	servant       = (POA_GNOME_Spell_Engine *) g_new0 (BonoboObjectServant, 1);
	servant->vepv = &engine_vepv;

	CORBA_exception_init (&ev);
	POA_GNOME_Spell_Engine__init ((PortableServer_Servant) servant, &ev);
	ORBIT_OBJECT_KEY (servant->_private)->object = NULL;

	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);

	return (GNOME_Spell_Engine) bonobo_object_activate_servant (engine, servant);
}

GNOMESpellEngine *
gnome_spell_engine_new (void)
{
	GNOMESpellEngine *engine;
	GNOME_Spell_Engine corba_engine;

              engine = gtk_type_new (gnome_spell_engine_get_type ());
	corba_engine = create_engine (BONOBO_OBJECT (engine));

	if (corba_engine == CORBA_OBJECT_NIL) {
		bonobo_object_unref (BONOBO_OBJECT (engine));
		return NULL;
	}
	
	return engine_construct (engine, corba_engine);
}
