#include <config.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "dict-stream.h"

static BonoboStreamClass *bonobo_stream_dict_parent_class;

static Bonobo_StorageInfo*
dict_get_info(BonoboStream *stream,
	      const Bonobo_StorageInfoFields mask,
	      CORBA_Environment *ev)
{
	g_warning ("Not implemented");

	return CORBA_OBJECT_NIL;
}

static void
dict_set_info(BonoboStream *stream,
	      const Bonobo_StorageInfo *info,
	      const Bonobo_StorageInfoFields mask,
	      CORBA_Environment *ev)
{
	g_warning ("Not implemented");
}

static void
dict_write(BonoboStream *stream, const Bonobo_Stream_iobuf *buffer,
	   CORBA_Environment *ev)
{
        CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, 
		ex_Bonobo_Stream_NotSupported, NULL);
}

static void
dict_read (BonoboStream         *stream,
	   CORBA_long            count,
	   Bonobo_Stream_iobuf **buffer,
	   CORBA_Environment    *ev)
{
	BonoboStreamDict *dict_stream = BONOBO_STREAM_DICT (stream);
	CORBA_octet *data;
	char *body;
	int len;
	int v;

	if (!dict_stream->data) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_IOError, NULL);
		return;
	}

	*buffer = Bonobo_Stream_iobuf__alloc();
	CORBA_sequence_set_release (*buffer, TRUE);
	data = CORBA_sequence_CORBA_octet_allocbuf (count);

	body = dict_stream->data + dict_stream->offset;
	len  = dict_stream->len  - dict_stream->offset;
	
	if (len > count) {
		memcpy (data, body, count);
		v = count;
	} else {
		memcpy (data, body, len);
		v = len;
	}
	
	dict_stream->offset += v;
	
	(*buffer)->_buffer = data;
	(*buffer)->_length = v;
}

static CORBA_long
dict_seek(BonoboStream *stream,
	  CORBA_long offset, Bonobo_Stream_SeekType whence,
	  CORBA_Environment *ev)
{
	BonoboStreamDict *dict_stream = BONOBO_STREAM_DICT(stream);

	switch (whence) {
	case Bonobo_Stream_SEEK_CUR:
		dict_stream->offset += offset;
		break;
	case Bonobo_Stream_SEEK_SET:
		dict_stream->offset = offset;
		break;
	case Bonobo_Stream_SEEK_END:
	default:
		CORBA_exception_set(
			ev, CORBA_USER_EXCEPTION, 
			ex_Bonobo_Stream_NotSupported, NULL);		
		return -1;
	}

	return dict_stream->offset;
}

static void
dict_truncate(BonoboStream *stream,
	      const CORBA_long new_size, 
	      CORBA_Environment *ev)
{
        CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, 
		ex_Bonobo_Stream_NotSupported, NULL);
}

static void
dict_copy_to(BonoboStream *stream,
	     const CORBA_char *dest,
	     const CORBA_long bytes,
	     CORBA_long *read_bytes,
	     CORBA_long *written_bytes,
	     CORBA_Environment *ev)
{
        CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, 
		ex_Bonobo_Stream_NotSupported, NULL);
}

static void
dict_commit(BonoboStream *stream,
	    CORBA_Environment *ev)
{
        CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, 
		ex_Bonobo_Stream_NotSupported, NULL);
}

static void
dict_revert(BonoboStream *stream,
	    CORBA_Environment *ev)
{
        CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, 
		ex_Bonobo_Stream_NotSupported, NULL);
}

static void
dict_destroy(GtkObject *object)
{
	BonoboStreamDict *dict_stream = BONOBO_STREAM_DICT(object);
	
}

static void
bonobo_stream_dict_class_init (BonoboStreamDictClass *klass)
{
	GtkObjectClass    *oclass = (GtkObjectClass *) klass;
	BonoboStreamClass *sclass = BONOBO_STREAM_CLASS(klass);
	
	bonobo_stream_dict_parent_class = gtk_type_class(
		bonobo_stream_get_type ());

	sclass->get_info = dict_get_info;
	sclass->set_info = dict_set_info;
	sclass->write    = dict_write;
	sclass->read     = dict_read;
	sclass->seek     = dict_seek;
	sclass->truncate = dict_truncate;
	sclass->copy_to  = dict_copy_to;
        sclass->commit   = dict_commit;
        sclass->revert   = dict_revert;

	oclass->destroy = dict_destroy;
}

GtkType
bonobo_stream_dict_get_type(void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"BonoboStreamDict",
			sizeof (BonoboStreamDict),
			sizeof (BonoboStreamDictClass),
			(GtkClassInitFunc) bonobo_stream_dict_class_init,
			(GtkObjectInitFunc) NULL,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique(bonobo_stream_get_type(), &info);
	}

	return type;
}

static gboolean
parse_url (BonoboStreamDict *s, const gchar *url)
{
	gchar *p, *slash, *address;

	g_free (s->host);

	s->host = NULL;
	s->port = 0;

	/* check for dict:// */
	if (strncmp (url, "dict://", 7))
		return FALSE;

	url += 7;

	/* check right syntaxe */
	if (!(slash = strchr (url, '/')))
		return FALSE;
	if (strlen (slash) < 4 || slash [2] != ':' || (slash [1] != 'w' && slash [1] != 'm'))
		return FALSE;

	address = g_strndup (url, slash - url);
	p = strchr (address, ':');
	if (p) {
		/* parse host:port */
		s->host = g_strndup (address, p - address);
		s->port = atoi (p + 1);
	} else {
		/* parse host */
		s->host = address;
		s->port = 2628;
		address = NULL;
	}
	g_free (address);

	s->cmd = g_strdup (slash + 1);

	return TRUE;
}

static gchar *
read_line (gint fd)
{
	GString *str = g_string_sized_new (128);
	gchar c;
	gchar *rv;

	while (read (fd, &c, 1) == 1) {
		g_string_append_c (str, c);
		if (c == '\n')
			break;
	}

	rv = str->str;
	g_string_free (str, FALSE);

	return rv;
}

static void
ensure_connection (BonoboStreamDict *s)
{
	if (s->socket < 0) {
  
		struct hostent  *host;
		struct protoent *protocol;
		struct sockaddr_in *address;
		gint option;

		address = g_new0 (struct sockaddr_in, 1);
		address->sin_family = AF_INET;
		address->sin_port   = htons((u_short) s->port);

		host = gethostbyname (s->host);
		if (!host)
			return;

		memcpy (&address->sin_addr, host->h_addr, host->h_length);
		protocol = getprotobyname("tcp");
		if (!protocol)
			return;
  
		s->socket = socket (PF_INET, SOCK_STREAM, protocol->p_proto);
		if (s->socket < 0)
			return;
  
		option = 1;
		setsockopt (s->socket, SOL_SOCKET, SO_REUSEADDR,  &option, sizeof (option));
  
		if (connect (s->socket, (struct sockaddr *) address, sizeof (struct sockaddr_in)) < 0)
			s->socket = -1;
		else {
			gboolean ok = FALSE;
			gchar *line;
#define CLIENT "CLIENT gnome-spell version " VERSION "\n"
			write (s->socket, CLIENT, strlen (CLIENT));
			while ((line = read_line (s->socket)) && *line)
				if (!strncmp (line, "250", 3)) {
					ok = TRUE;
					break;
				}
			if (!ok) {
				close (s->socket);
				s->socket = -1;
			} else {
				g_free (s->connected_host);
				s->connected_host = g_strdup (s->host);
				s->connected_port = s->port;
			}
		}
	}
}

inline static GString *
g_string_append_len(GString *st, const char *s, int l)
{
	char *tmp;

	tmp = alloca(l+1);
	tmp[l]=0;
	memcpy(tmp, s, l);
	return g_string_append(st, tmp);
}

static void
add_line (GString *str, const gchar *line, gboolean *in_table, gboolean *first, gboolean *cite)
{
	gchar *s, *s2;

	if (strlen (line) > 5 && line [5] != ' ' && (s = strchr (line, ':'))) {
		gchar *s1 = g_strndup (line + 5, s - line - 4);

		if (!strchr (s1, '[')) {

			g_string_append (str, *in_table ? "</TD></TR>" : "<TABLE>");
			g_string_append (str, "<TR><TD VALIGN=\"top\" ALIGN=\"right\"><FONT COLOR=\"#FF0000\">");

			while (*s1) {
				if (*s1 == ' ')
					g_string_append (str, "&nbsp;");
				else
					g_string_append_c (str, *s1);
				s1 ++;
			}

			g_string_append (str, "</FONT></TD><TD>");

			line = s + 1;
			*in_table = TRUE;
		}
	}

	if ((s = strchr (line, '"'))) {
		while (s) {
			g_string_append_len (str, line, s - line);
			g_string_append (str, *cite ? "</I>\"" : "\"<I>");
			*cite = *cite ? FALSE : TRUE;

			line = s + 1;
			s = strchr (line, '"');
		}
	}

	while ((s = strchr (line, '{')) || (s = strchr (line, '}'))) {
		gchar *s1;

		if (*s == '{' && (s2 = strchr (line, '}')) && s2 < s)
		    s = s2;
		s1 = strchr (s, *s == '{' ? '}' : '{');

		g_string_append_len (str, line, s - line);
		g_string_append (str, *s == '{' ? "<A HREF=\"dict://dict.org/w:" : "</A>");
		if (*s == '{') {
			if (s1)
				g_string_append_len (str, s + 1, (s1 - s) - 1);
			else
				g_string_append (str, s + 1);
			g_string_append (str, "\">");
		}
		if (s1) {
			g_string_append_len (str, s + 1, (s1 - s) - 1);
			line = s1;
		} else {
			line = s + 1;
			break;
		}
	}

	if (*first) {
		g_string_append (str, "<B>");
		g_string_append (str, line);
		g_string_append (str, "</B><HR SIZE=\"1\">");
		*first = FALSE;
	} else
		g_string_append (str, line);
}

static gboolean
read_data (BonoboStreamDict *s)
{
	GString *str;
	gchar *word;
	gboolean rv = FALSE;

	/* FIXME add more cmds */
	g_assert (!strncmp (s->cmd, "w:", 2));

	word = s->cmd + 2;
	/* printf ("lookup: %s\n", word); */

	g_free (s->data);

	if (s->socket >= 0) {
		gchar *line;
		gboolean in_table = FALSE, first = TRUE, cite = FALSE;

		line = g_strdup_printf ("DEFINE wn \"%s\"\n", word);
		/* printf ("send: %s", line); */
		write (s->socket, line, strlen (line));
		g_free (line);

		str  = g_string_sized_new (256);
		while ((line = read_line (s->socket)) && *line) {
			/* printf ("line: %s", line); */

			/* Invalid DB */
			if (!strncmp (line, "550", 3))
				break;
			/* No match */
			else if (!strncmp (line, "552", 3)) {
				g_string_append (str, "<B>");
				g_string_append (str, word);
				g_string_append (str,
						 "</B><HR SIZE=\"1\"><TABLE><TR><TD>"
						 "No description found."
						 "</TD></TR></TABLE>\n");
				rv = TRUE;
				break;
			/* n definitions */
			} else if (!strncmp (line, "150", 3)) {
				;
			/* word database name */
			} else if (!strncmp (line, "151", 3))
				;
			/* OK */
			else if (!strncmp (line, "250", 3)) {
				g_string_append (str, "</TD></TR></TABLE>\n");
				rv = TRUE;
				break;
			} else {
				if (*line != '.')
					add_line (str, line, &in_table, &first, &cite);
			}
			g_free (line);
			line = NULL;
		}
		g_free (line);

	} else
		return FALSE;

	s->data   = rv ? str->str : NULL;
	s->len    = rv ? strlen (s->data) : -1;
	s->offset = 0;
	g_string_free (str, !rv);

	/* printf ("data readed: %s\n", s->data); */

	return TRUE;
}

static void
stream_disconnect (BonoboStreamDict *s)
{
	if (s->socket >= 0)
		close (s->socket);

	g_free (s->host);
	s->host   = NULL;
	s->port   = 0;
	s->socket = 0;
}

static gboolean
stream_connect (BonoboStreamDict *s, const gchar *url)
{
	if (!parse_url (s, url))
		return FALSE;

	if (s->connected_host) {
		if (!strcmp (s->host, s->connected_host) && s->port == s->connected_port)
			return TRUE;
		else
			stream_disconnect (s);
	}

	ensure_connection (s);

	return s->socket >= 0 ? TRUE : FALSE;
}

static gboolean
open_and_read (BonoboStreamDict *s, const gchar *url)
{
	if (!stream_connect (s, url))
		return FALSE;

	read_data (s);
}

BonoboStream *
bonobo_stream_dict_new (const CORBA_char *url)
{
	BonoboStreamDict *stream;
	Bonobo_Stream corba_stream;

	g_return_val_if_fail (url != NULL, NULL);

	stream = gtk_type_new (bonobo_stream_dict_get_type());
	if (stream == NULL)
		return NULL;

	corba_stream = bonobo_stream_corba_object_create (BONOBO_OBJECT (stream));

	if (corba_stream == CORBA_OBJECT_NIL) {
		bonobo_object_unref(BONOBO_OBJECT(stream));
		return NULL;
	}

	stream->socket = -1;

	return open_and_read (stream, url)
		? BONOBO_STREAM (bonobo_object_construct (BONOBO_OBJECT (stream), corba_stream))
		: NULL;
}

BonoboStream *
bonobo_stream_dict_open (const char *url, gint flags, gint mode, CORBA_Environment *ev)
{
	return bonobo_stream_dict_new (url);
}
