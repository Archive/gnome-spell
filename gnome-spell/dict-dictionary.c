/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  This file is part of gnome-spell bonobo component
    copied from echo.c written by Miguel de Icaza and updated for Spell.idl needs

    Copyright (C) 1999, 2000 Helix Code, Inc.
    Authors:                 Radek Doulik <rodo@helixcode.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include <config.h>
#include <bonobo.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "Dictionary.h"
#include "dict-dictionary.h"

static BonoboObjectClass            *dictionary_parent_class;
static POA_GNOME_Dictionary__vepv    dictionary_vepv;

static void
dictionary_object_init (GtkObject *object)
{
	GNOMEDictionary *dict = GNOME_DICTIONARY (object);

	dict->socket = -1;
}

static void
dictionary_object_destroy (GtkObject *object)
{
	GNOMEDictionary *dictionary = GNOME_DICTIONARY (object);

	GTK_OBJECT_CLASS (dictionary_parent_class)->destroy (object);
}

/**
 * methods implementation
 */

inline static GNOMEDictionary *
dictionary_from_servant (PortableServer_Servant _servant)
{
	return GNOME_DICTIONARY (bonobo_object_from_servant (_servant));
}

static void
impl_disconnect (PortableServer_Servant _servant, CORBA_Environment * ev)
{
}

#define PORT 2628
#define HOST "dict.org"

static gchar *
read_line (gint fd)
{
	GString *str = g_string_sized_new (128);
	gchar c;
	gchar *rv;

	while (read (fd, &c, 1) == 1) {
		g_string_append_c (str, c);
		if (c == '\n')
			break;
	}

	rv = str->str;
	g_string_free (str, FALSE);

	return rv;
}

static void
ensure_connection (GNOMEDictionary *d)
{
	if (d->socket < 0) {
  
		struct hostent  *host;
		struct protoent *protocol;
		struct sockaddr_in *address;
		gint option;

		address = g_new0 (struct sockaddr_in, 1);
		address->sin_family = AF_INET;
		address->sin_port   = htons((u_short) PORT);

		host = gethostbyname (HOST);
		if (!host)
			return;

		memcpy (&address->sin_addr, host->h_addr, host->h_length);
		protocol = getprotobyname("tcp");
		if (!protocol)
			return;
  
		d->socket = socket (PF_INET, SOCK_STREAM, protocol->p_proto);
		if (d->socket < 0)
			return;
  
		option = 1;
		setsockopt (d->socket, SOL_SOCKET, SO_REUSEADDR,  &option, sizeof (option));
  
		if (connect (d->socket, (struct sockaddr *) address, sizeof (struct sockaddr_in)) < 0)
			d->socket = -1;
		else {
			gboolean ok = FALSE;
			gchar *line;
#define CLIENT "CLIENT gnome-spell version " VERSION "\n"
			write (d->socket, CLIENT, strlen (CLIENT));
			while ((line = read_line (d->socket)) && *line)
				if (!strncmp (line, "250", 3)) {
					ok = TRUE;
					break;
				}
			if (!ok) {
				close (d->socket);
				d->socket = -1;
			}
		}
	}
}

inline static GString *
g_string_append_len(GString *st, const char *s, int l)
{
	char *tmp;

	tmp = alloca(l+1);
	tmp[l]=0;
	memcpy(tmp, s, l);
	return g_string_append(st, tmp);
}

static void
add_line (GString *str, const gchar *line, gboolean *in_table, gboolean *first, gboolean *cite)
{
	gchar *s, *s2;

	if (strlen (line) > 5 && line [5] != ' ' && (s = strchr (line, ':'))) {
		gchar *s1 = g_strndup (line + 5, s - line - 4);

		if (!strchr (s1, '[')) {

			g_string_append (str, *in_table ? "</TD></TR>" : "<TABLE>");
			g_string_append (str, "<TR><TD VALIGN=\"top\" ALIGN=\"right\"><FONT COLOR=\"#FF0000\">");

			while (*s1) {
				if (*s1 == ' ')
					g_string_append (str, "&nbsp;");
				else
					g_string_append_c (str, *s1);
				s1 ++;
			}

			g_string_append (str, "</FONT></TD><TD>");

			line = s + 1;
			*in_table = TRUE;
		}
	}

	if ((s = strchr (line, '"'))) {
		while (s) {
			g_string_append_len (str, line, s - line);
			g_string_append (str, *cite ? "</I>\"" : "\"<I>");
			*cite = *cite ? FALSE : TRUE;

			line = s + 1;
			s = strchr (line, '"');
		}
	}

	while ((s = strchr (line, '{')) || (s = strchr (line, '}'))) {
		gchar *s1;

		if (*s == '{' && (s2 = strchr (line, '}')) && s2 < s)
		    s = s2;
		s1 = strchr (s, *s == '{' ? '}' : '{');

		g_string_append_len (str, line, s - line);
		g_string_append (str, *s == '{' ? "<A HREF=\"dict://dict.org/d:" : "</A>");
		if (*s == '{') {
			if (s1)
				g_string_append_len (str, s + 1, (s1 - s) - 1);
			else
				g_string_append (str, s + 1);
			g_string_append (str, "\">");
		}
		if (s1) {
			g_string_append_len (str, s + 1, (s1 - s) - 1);
			line = s1;
		} else {
			line = s + 1;
			break;
		}
	}

	if (*first) {
		g_string_append (str, "<B>");
		g_string_append (str, line);
		g_string_append (str, "</B><HR SIZE=\"1\">");
		*first = FALSE;
	} else
		g_string_append (str, line);
}

static CORBA_char *
impl_lookup_html (PortableServer_Servant _servant, const CORBA_char * word, CORBA_Environment * ev)
{
	GNOMEDictionary *d = dictionary_from_servant (_servant);
	CORBA_char *rv;
	gchar *html = NULL;

	/* printf ("lookup: %s\n", word); */

	ensure_connection (d);
	if (d->socket >= 0) {
		GString *str = g_string_sized_new (256);
		gchar *line;
		gboolean in_table = FALSE, first = TRUE, cite = FALSE;

		line = g_strdup_printf ("DEFINE wn %s\n", word);
		/* printf ("send: %s", line); */
		write (d->socket, line, strlen (line));
		g_free (line);

		while ((line = read_line (d->socket)) && *line) {
			/* printf ("line: %s", line); */

			/* Invalid DB */
			if (!strncmp (line, "550", 3))
				break;
			/* No match */
			else if (!strncmp (line, "552", 3)) {
				g_string_append (str, "<B>");
				g_string_append (str, word);
				g_string_append (str,
						 "</B><HR SIZE=\"1\"><TABLE><TR><TD>"
						 "No description found."
						 "</TD></TR></TABLE>");
				break;
			/* n definitions */
			} else if (!strncmp (line, "150", 3)) {
				;
			/* word database name */
			} else if (!strncmp (line, "151", 3))
				;
			/* OK */
			else if (!strncmp (line, "250", 3)) {
				g_string_append (str, "</TD></TR></TABLE>");
				break;
			} else {
				add_line (str, line, &in_table, &first, &cite);
			}
			g_free (line);
			line = NULL;
		}
		g_free (line);

		html = str->str;
		g_string_free (str, FALSE);
	}

	if (!html)
		html = g_strdup ("");

	rv = CORBA_string_dup (html);
	g_free (html);

	return rv;
}

/*
 * if you want users to derive classes from your implementation
 * you need to support this method.
 */
POA_GNOME_Dictionary__epv *
gnome_dictionary_get_epv (void)
{
	POA_GNOME_Dictionary__epv *epv;

	epv = g_new0 (POA_GNOME_Dictionary__epv, 1);

	epv->lookup_html = impl_lookup_html;

	return epv;
}

static void
init_dictionary_corba_class (void)
{
	dictionary_vepv.Bonobo_Unknown_epv         = bonobo_object_get_epv ();
	dictionary_vepv.GNOME_Dictionary_epv = gnome_dictionary_get_epv ();
}

static void
dictionary_class_init (GNOMEDictionaryClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	dictionary_parent_class  = gtk_type_class (bonobo_object_get_type ());
	object_class->destroy    = dictionary_object_destroy;

	init_dictionary_corba_class ();
}

GtkType
gnome_dictionary_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"Dictionary",
			sizeof (GNOMEDictionary),
			sizeof (GNOMEDictionaryClass),
			(GtkClassInitFunc) dictionary_class_init,
			(GtkObjectInitFunc) dictionary_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type (), &info);
	}

	return type;
}

GNOMEDictionary *
dictionary_construct (GNOMEDictionary *dictionary, GNOME_Dictionary corba_dictionary)
{
	CORBA_Environment ev;
	static int i;
	
	g_return_val_if_fail (dictionary != NULL, NULL);
	g_return_val_if_fail (IS_GNOME_DICTIONARY (dictionary), NULL);
	g_return_val_if_fail (corba_dictionary != CORBA_OBJECT_NIL, NULL);

	if (!bonobo_object_construct (BONOBO_OBJECT (dictionary), (CORBA_Object) corba_dictionary))
		return NULL;

	CORBA_exception_init (&ev);
	CORBA_exception_free (&ev);

	return dictionary;
}

/*
 * This routine creates the ORBit CORBA server and initializes the
 * CORBA side of things
 */
static GNOME_Dictionary
create_dictionary (BonoboObject *dictionary)
{
	POA_GNOME_Dictionary *servant;
	CORBA_Environment ev;

	servant       = (POA_GNOME_Dictionary *) g_new0 (BonoboObjectServant, 1);
	servant->vepv = &dictionary_vepv;

	CORBA_exception_init (&ev);
	POA_GNOME_Dictionary__init ((PortableServer_Servant) servant, &ev);
	ORBIT_OBJECT_KEY (servant->_private)->object = NULL;

	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);

	return (GNOME_Dictionary) bonobo_object_activate_servant (dictionary, servant);
}

GNOMEDictionary *
gnome_dictionary_new (void)
{
	GNOMEDictionary *dictionary;
	GNOME_Dictionary corba_dictionary;

              dictionary = gtk_type_new (gnome_dictionary_get_type ());
	corba_dictionary = create_dictionary (BONOBO_OBJECT (dictionary));

	if (corba_dictionary == CORBA_OBJECT_NIL) {
		bonobo_object_unref (BONOBO_OBJECT (dictionary));
		return NULL;
	}
	
	return dictionary_construct (dictionary, corba_dictionary);
}
