#ifndef _BONOBO_STREAM_DICT_H_
#define _BONOBO_STREAM_DICT_H_

#include <bonobo/bonobo-stream.h>

BEGIN_GNOME_DECLS

#define BONOBO_STREAM_DICT_TYPE        (bonobo_stream_dict_get_type ())
#define BONOBO_STREAM_DICT(o)          (GTK_CHECK_CAST ((o), BONOBO_STREAM_DICT_TYPE, BonoboStreamDict))
#define BONOBO_STREAM_DICT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_STREAM_DICT_TYPE, BonoboStreamDictClass))
#define BONOBO_IS_STREAM_DICT(o)       (GTK_CHECK_TYPE ((o), BONOBO_STREAM_DICT_TYPE))
#define BONOBO_IS_STREAM_DICT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_STREAM_DICT_TYPE))

typedef struct _BonoboStreamDict     BonoboStreamDict;

struct _BonoboStreamDict {
	BonoboStream stream;

	gint     socket;
	gchar   *host;
	guint    port;
	gchar   *connected_host;
	guint    connected_port;

	gchar   *cmd;
	gchar   *data;
	gint     len;

	gboolean html;
	guint    offset;
};

typedef struct {
	BonoboStreamClass parent_class;
} BonoboStreamDictClass;

GtkType          bonobo_stream_dict_get_type     (void);
BonoboStream    *bonobo_stream_dict_open         (const char *path, gint flags,
						  gint mode, CORBA_Environment *ev);
BonoboStream    *bonobo_stream_dict_create       (const CORBA_char *path);
BonoboStream    *bonobo_stream_dict_construct    (BonoboStreamDict *stream,
						  Bonobo_Stream corba_stream);
	
END_GNOME_DECLS

#endif /* _BONOBO_STREAM_DICT_H_ */
