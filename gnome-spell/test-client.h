/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  This file is part of gnome-spell bonobo component

    Copyright (C) 1999, 2000 Helix Code, Inc.
    Authors:                 Radek Doulik <rodo@helixcode.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef SPELL_CLIENT_H_
#define SPELL_CLIENT_H_

#include <libgnome/gnome-defs.h>
#include <bonobo/bonobo-object.h>
#include <pspell/pspell.h>

BEGIN_GNOME_DECLS

#define GNOME_SPELL_CLIENT_TYPE        (gnome_spell_client_get_type ())
#define GNOME_SPELL_CLIENT(o)          (GTK_CHECK_CAST ((o),       GNOME_SPELL_CLIENT_TYPE, GNOMESpellClient))
#define GNOME_SPELL_CLIENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k),  GNOME_SPELL_CLIENT_TYPE, GNOMESpellClientClass))
#define IS_GNOME_SPELL_CLIENT(o)       (GTK_CHECK_TYPE ((o),       GNOME_SPELL_CLIENT_TYPE))
#define IS_GNOME_SPELL_CLIENT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_SPELL_CLIENT_TYPE))

typedef struct {
	BonoboObject parent;

	GtkWidget *dialog;
} GNOMESpellClient;

typedef struct {
	BonoboObjectClass parent_class;
} GNOMESpellClientClass;

GtkType                          gnome_spell_client_get_type   (void);
GNOMESpellClient            *gnome_spell_client_construct  (GNOMESpellClient   *client,
								    GNOME_Spell_Client  corba_client);
GNOMESpellClient            *gnome_spell_client_new        (void);
POA_GNOME_Spell_Client__epv *gnome_spell_client_get_epv    (void);

END_GNOME_DECLS

#endif
