/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  This file is part of gnome-spell bonobo component

    Copyright (C) 1999, 2000 Helix Code, Inc.
    Authors:                 Radek Doulik <rodo@helixcode.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef SPELL_ENGINE_H_
#define SPELL_ENGINE_H_

#include <libgnome/gnome-defs.h>
#include <bonobo/bonobo-object.h>
#include <pspell/pspell.h>
#include "Spell.h"
#include "Dictionary.h"

BEGIN_GNOME_DECLS

#define GNOME_SPELL_ENGINE_TYPE        (gnome_spell_engine_get_type ())
#define GNOME_SPELL_ENGINE(o)          (GTK_CHECK_CAST ((o),       GNOME_SPELL_ENGINE_TYPE, GNOMESpellEngine))
#define GNOME_SPELL_ENGINE_CLASS(k)    (GTK_CHECK_CLASS_CAST((k),  GNOME_SPELL_ENGINE_TYPE, GNOMESpellEngineClass))
#define IS_GNOME_SPELL_ENGINE(o)       (GTK_CHECK_TYPE ((o),       GNOME_SPELL_ENGINE_TYPE))
#define IS_GNOME_SPELL_ENGINE_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_SPELL_ENGINE_TYPE))

typedef struct {
	BonoboObject parent;

	BonoboObjectClient *dictionary_client;
	BonoboObjectClient *client_client;
	BonoboObjectClient *words_db_client;

	GNOME_Spell_Dictionary dictionary;
	GNOME_Spell_Client     client;
	GNOME_Dictionary       words_db;

	GtkWidget *dialog;
	GtkWidget *word_entry;
	GtkWidget *suggestions_clist;
	GtkWidget *status_label;
	GtkWidget *sw_html;
	GtkWidget *description_gtkhtml;

	gint selected_row;
	gint words;
	gchar **word_description;
} GNOMESpellEngine;

typedef struct {
	BonoboObjectClass parent_class;
} GNOMESpellEngineClass;

GtkType                      gnome_spell_engine_get_type   (void);
GNOMESpellEngine            *gnome_spell_engine_construct  (GNOMESpellEngine   *engine,
							    GNOME_Spell_Engine  corba_engine);
GNOMESpellEngine            *gnome_spell_engine_new        (void);
POA_GNOME_Spell_Engine__epv *gnome_spell_engine_get_epv    (void);

END_GNOME_DECLS

#endif
