/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  This file is part of gnome-spell bonobo component
    copied from echo.c written by Miguel de Icaza and updated for Spell.idl needs

    Copyright (C) 1999, 2000 Helix Code, Inc.
    Authors:                 Radek Doulik <rodo@helixcode.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include <config.h>
#include <bonobo.h>

#include <glade/glade.h>

#include "Spell.h"
#include "test-client.h"

static BonoboObjectClass              *client_parent_class;
static POA_GNOME_Spell_Client__vepv    client_vepv;

static void
client_object_init (GtkObject *object)
{
	GNOMESpellClient *dict = GNOME_SPELL_CLIENT (object);

	dict->dialog = NULL;
}

static void
client_object_destroy (GtkObject *object)
{
	GNOMESpellClient *client = GNOME_SPELL_CLIENT (object);

	GTK_OBJECT_CLASS (client_parent_class)->destroy (object);
}

/**
 * methods implementation
 */

#define WORDS 5
static gint words = WORDS;
static gchar *word [WORDS] = { "youy", "aare", "How", "dude", "Heloo" };

static CORBA_char *
impl_next_word (PortableServer_Servant _servant, CORBA_Environment * ev)
{
	if (words) {
		words --;
		printf ("next word (%d): %s\n", words, word [words]);
		return CORBA_string_dup (word [words]);
	} else {
		printf ("EOF\n");
		return CORBA_string_dup ("");
	}
}

static void
impl_replace (PortableServer_Servant _servant, const CORBA_char * replacement, CORBA_Environment * ev)
{
	printf ("Replace with: %s\n", replacement);
}

static void
impl_remembered (PortableServer_Servant _servant, CORBA_Environment * ev)
{
	printf ("This word was remebered.\n");
}

static void
impl_ignored (PortableServer_Servant _servant, CORBA_Environment * ev)
{
	printf ("This word was ignored.\n");
}

/*
 * If you want users to derive classes from your implementation
 * you need to support this method.
 */
POA_GNOME_Spell_Client__epv *
gnome_spell_client_get_epv (void)
{
	POA_GNOME_Spell_Client__epv *epv;

	epv = g_new0 (POA_GNOME_Spell_Client__epv, 1);

	epv->nextWord   = impl_next_word;
	epv->replace    = impl_replace;
	epv->remembered = impl_remembered;
	epv->ignored    = impl_ignored;

	return epv;
}

static void
init_client_corba_class (void)
{
	client_vepv.Bonobo_Unknown_epv         = bonobo_object_get_epv ();
	client_vepv.GNOME_Spell_Client_epv = gnome_spell_client_get_epv ();
}

static void
client_class_init (GNOMESpellClientClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	client_parent_class  = gtk_type_class (bonobo_object_get_type ());
	object_class->destroy    = client_object_destroy;

	init_client_corba_class ();
}

GtkType
gnome_spell_client_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"Client",
			sizeof (GNOMESpellClient),
			sizeof (GNOMESpellClientClass),
			(GtkClassInitFunc) client_class_init,
			(GtkObjectInitFunc) client_object_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type (), &info);
	}

	return type;
}

GNOMESpellClient *
client_construct (GNOMESpellClient *client, GNOME_Spell_Client corba_client)
{
	CORBA_Environment ev;
	static int i;
	
	g_return_val_if_fail (client != NULL, NULL);
	g_return_val_if_fail (IS_GNOME_SPELL_CLIENT (client), NULL);
	g_return_val_if_fail (corba_client != CORBA_OBJECT_NIL, NULL);

	if (!bonobo_object_construct (BONOBO_OBJECT (client), (CORBA_Object) corba_client))
		return NULL;

	CORBA_exception_init (&ev);
	CORBA_exception_free (&ev);

	return client;
}

/*
 * This routine creates the ORBit CORBA server and initializes the
 * CORBA side of things
 */
static GNOME_Spell_Client
create_client (BonoboObject *client)
{
	POA_GNOME_Spell_Client *servant;
	CORBA_Environment ev;

	servant       = (POA_GNOME_Spell_Client *) g_new0 (BonoboObjectServant, 1);
	servant->vepv = &client_vepv;

	CORBA_exception_init (&ev);
	POA_GNOME_Spell_Client__init ((PortableServer_Servant) servant, &ev);
	ORBIT_OBJECT_KEY (servant->_private)->object = NULL;

	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);

	return (GNOME_Spell_Client) bonobo_object_activate_servant (client, servant);
}

GNOMESpellClient *
gnome_spell_client_new (void)
{
	GNOMESpellClient *client;
	GNOME_Spell_Client corba_client;

              client = gtk_type_new (gnome_spell_client_get_type ());
	corba_client = create_client (BONOBO_OBJECT (client));

	if (corba_client == CORBA_OBJECT_NIL) {
		bonobo_object_unref (BONOBO_OBJECT (client));
		return NULL;
	}
	
	return client_construct (client, corba_client);
}
