/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * bonobo-storage-dict.c: DICT Storage implementation
 *
 * A libgdict-based Storage implementation. Based around the fs storage impl.
 *
 * Copyright (c) 2000 Helix Code, Inc.
 *
 * Author:
 *   Joe Shaw (joe@helixcode.com)
 */

#include <config.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include <bonobo/bonobo-storage-plugin.h>
#include "dict-storage.h"
#include "dict-stream.h"

static BonoboStorageClass *bonobo_storage_dict_parent_class;

static void
bonobo_storage_dict_destroy (GtkObject *object)
{
	BonoboStorageDict *storage = BONOBO_STORAGE_DICT(object);
}

static Bonobo_StorageInfo*
dict_get_info (BonoboStorage *storage,
	       const CORBA_char *path,
	       const Bonobo_StorageInfoFields mask,
	       CORBA_Environment *ev)
{
	g_warning ("Not implemented");

	return CORBA_OBJECT_NIL;
}

static void
dict_set_info(BonoboStorage *storage,
	      const CORBA_char *path,
	      const Bonobo_StorageInfo *info,
	      const Bonobo_StorageInfoFields mask,
	      CORBA_Environment *ev)
{
	CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_NotSupported, NULL);
} /* dict_set_info */

static BonoboStream *
dict_open_stream(BonoboStorage *storage, const CORBA_char *url, 
		 Bonobo_Storage_OpenMode mode, CORBA_Environment *ev)
{
	BonoboStream *stream;

	stream = bonobo_stream_dict_open(url, mode, 0644, ev);

	return stream;
} /* dict_open_stream */

static BonoboStorage *
dict_open_storage(BonoboStorage *storage, const CORBA_char *url, 
		  Bonobo_Storage_OpenMode mode, CORBA_Environment *ev)
{
	BonoboStorage *new_storage;

	new_storage = bonobo_storage_dict_open (url, mode, 0644, ev);

	return new_storage;
} /* dict_open_storage */

static void
dict_copy_to(BonoboStorage *storage, Bonobo_Storage target, 
	     CORBA_Environment *ev)
{
	CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_NotSupported, NULL);
} /* dict_copy_to */

static void
dict_rename(BonoboStorage *storage, const CORBA_char *path, 
	    const CORBA_char *new_path, CORBA_Environment *ev)
{
	CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_NotSupported, NULL);
} /* dict_rename */

static void
dict_commit(BonoboStorage *storage, CORBA_Environment *ev)
{
	CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_NotSupported, NULL);
} /* dict_commit */

static void
dict_revert(BonoboStorage *storage, CORBA_Environment *ev)
{
	CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_NotSupported, NULL);
} /* dict_revert */

static Bonobo_Storage_DirectoryList *
dict_list_contents(BonoboStorage *storage, const CORBA_char *path, 
		   Bonobo_StorageInfoFields mask, CORBA_Environment *ev)
{
	CORBA_exception_set(
		ev, CORBA_USER_EXCEPTION, ex_Bonobo_Stream_NotSupported, NULL);

	return NULL;
} /* dict_list_contents */

static void
bonobo_storage_dict_class_init(BonoboStorageDictClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;
	BonoboStorageClass *sclass = BONOBO_STORAGE_CLASS (klass);
	
	bonobo_storage_dict_parent_class = 
		gtk_type_class (bonobo_storage_get_type ());

	sclass->get_info       = dict_get_info;
	sclass->set_info       = dict_set_info;
	sclass->open_stream    = dict_open_stream;
	sclass->open_storage   = dict_open_storage;
	sclass->copy_to        = dict_copy_to;
	sclass->rename         = dict_rename;
	sclass->commit         = dict_commit;
	sclass->revert         = dict_revert;
	sclass->list_contents  = dict_list_contents;
	
	object_class->destroy = bonobo_storage_dict_destroy;
} /* bonobo_storage_dict_class_init */

static void
bonobo_storage_init(BonoboObject *object)
{
} /* bonobo_storage_init */

GtkType
bonobo_storage_dict_get_type(void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"IDL:GNOME/StorageDict:1.0",
			sizeof (BonoboStorageDict),
			sizeof (BonoboStorageDictClass),
			(GtkClassInitFunc) bonobo_storage_dict_class_init,
			(GtkObjectInitFunc) bonobo_storage_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_storage_get_type (), &info);
	}

	return type;
} /* bonobo_storage_dict_get_type */

/** 
 * bonobo_storage_dict_open:
 * @url: URL that represents the storage
 * @flags: open flags.
 * @mode: mode used if @flags containst Bonobo_Storage_CREATE for the storage.
 * @ev: A corba environment for exception handling.
 *
 * Returns a BonoboStorage object that represents the storage at @path
 */
BonoboStorage *
bonobo_storage_dict_open(const char *url, gint flags, gint mode, CORBA_Environment *ev)
{
	BonoboStorageDict *storage;
	Bonobo_Storage corba_storage;

	g_return_val_if_fail (url != NULL, NULL);

	if (flags & Bonobo_Storage_CREATE || flags & Bonobo_Storage_WRITE)
		return NULL;
	
	storage = gtk_type_new(bonobo_storage_dict_get_type());

	corba_storage = bonobo_storage_corba_object_create(
		BONOBO_OBJECT(storage));
	if (corba_storage == CORBA_OBJECT_NIL) {
		bonobo_object_unref(BONOBO_OBJECT(storage));
		return NULL;
	}

	return bonobo_storage_construct(
		BONOBO_STORAGE(storage), corba_storage);
}

gint 
init_storage_plugin(StoragePlugin *plugin)
{
	g_return_val_if_fail (plugin != NULL, -1);

	plugin->name = "dict";
	plugin->description = "Dictionary driver";
	plugin->version = BONOBO_STORAGE_VERSION;
	
	plugin->storage_open = bonobo_storage_dict_open; 
	plugin->stream_open  = bonobo_stream_dict_open; 

	return 0;
}
