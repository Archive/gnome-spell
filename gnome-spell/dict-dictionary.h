/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  This file is part of gnome-spell bonobo component

    Copyright (C) 1999, 2000 Helix Code, Inc.
    Authors:                 Radek Doulik <rodo@helixcode.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef DICT_DICTIONARY_H_
#define DICT_DICTIONARY_H_

#include <libgnome/gnome-defs.h>
#include <bonobo/bonobo-object.h>
#include <pspell/pspell.h>
#include "Dictionary.h"

BEGIN_GNOME_DECLS

#define GNOME_DICTIONARY_TYPE        (gnome_dictionary_get_type ())
#define GNOME_DICTIONARY(o)          (GTK_CHECK_CAST ((o),       GNOME_DICTIONARY_TYPE, GNOMEDictionary))
#define GNOME_DICTIONARY_CLASS(k)    (GTK_CHECK_CLASS_CAST((k),  GNOME_DICTIONARY_TYPE, GNOMEDictionaryClass))
#define IS_GNOME_DICTIONARY(o)       (GTK_CHECK_TYPE ((o),       GNOME_DICTIONARY_TYPE))
#define IS_GNOME_DICTIONARY_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_DICTIONARY_TYPE))

typedef struct {
	BonoboObject parent;

	gint socket;

} GNOMEDictionary;

typedef struct {
	BonoboObjectClass parent_class;
} GNOMEDictionaryClass;

GtkType                    gnome_dictionary_get_type   (void);
GNOMEDictionary           *gnome_dictionary_construct  (GNOMEDictionary  *engine,
							GNOME_Dictionary  corba_engine);
GNOMEDictionary           *gnome_dictionary_new        (void);
POA_GNOME_Dictionary__epv *gnome_dictionary_get_epv    (void);

END_GNOME_DECLS

#endif
